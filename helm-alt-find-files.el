;;; -*- mode:emacs-lisp -*-
;;; ================================================================================
;;; File:    helm-alt-find-files.el
;;; Date:    2020-10-15
;;; Author:  Void Your Warranty
;;; License: Gnu General Public License GPLv3 (https://www.gnu.org/licenses/gpl-3.0.en.html)
;;; Purpose: Alternative to helm-find-files that affords bash like TAB completion
;;; ================================================================================

(require 'helm)
(require 'helm-config)
(require 'helm-files) ;; required for helm-do-grep-1
(require 'helm-grep)  ;; required for helm-do-grep-1

(defcustom hf-recentf 5
  "The number of recently opened files to present in the helm buffer.")

;; Define a helm persistent action that provides a bash style [TAB] completion of paths.
;; -------------------------------------------------------------------------------------

(defun hf-common-prefix-two (a b)
  "Return the longest common prefix of the two given strings."
  (let ((result (compare-strings a nil nil b nil nil)))
    (if (numberp result)
	(substring a 0 (- (abs result) 1))
      a)))

(defun hf-common-prefix-list (l)
  "Return the longest common prefix of the strings in the given list."
  (if (not l) ""
    (let ((common (car l)))
      (catch 'result
	(dolist (try (cdr l))
	  (setq common (hf-common-prefix-two common try))
	  (when (string= common "")
	    (throw 'result ""))))
      common)))

(defun hf-persistent-action (candidate choices)
  "[TAB] completion during the helm selection of files is
  implemented by this persistent action. Here <candidate> is the
  currently highlighted file name and <choices> the list of all
  file names displayed."
  ;; The present functions requires a variable <previous> in order to detect whether the [TAB] key is pressed twice.
  (let* (
	 ;; what the user has typed
	 (typed helm-pattern)

	 ;; all file names whose beginning agrees with what the user has typed
	 (matches  (remove nil (mapcar (lambda (x) (when (string-prefix-p typed x) x)) choices)))

	 ;; local function that replaces the search string in the helm minibuffer
	 (insert-in-minibuffer (lambda (fname)
				 (with-selected-window (or (active-minibuffer-window) (minibuffer-window))
				   (delete-minibuffer-contents)
				   (set-text-properties 0 (length fname) nil fname)
				   (insert fname)))))

    ;; If the user has typed in something entirely new, there is not even a helm candidate, and this persistent action
    ;; function is never called. The fact that it is called, implies that there is a valid candidate (the currently
    ;; highlighted line).

;;;    (when (not matches)
;;;      ....)
;;;
;;;    In this situation, if the helm buffer has been narrowed down to a single candidate, we can insert its name into the minibuffer, too.
;;;

    (if (string= helm-pattern previous)
	;; If the [TAB] key was pressed twice (i.e. the contents of the minibuffer haven't changed since the previous call of this function),
	;; then copy the currently highlighed candidate into the minibuffer.
	(funcall insert-in-minibuffer (hf-resolve-arrow candidate))

      (when matches
	;; If the helm pattern in the minibuffer is a prefix to some choices.
	(let ((value (if (= 1 (length matches))
			 ;; If there is a unique match, this is the value we copy into the minibuffer.
			 (car matches)

		       ;; If there are several matches, we copy their common prefix into the minibuffer.
		       (hf-common-prefix-list matches))))
	  (funcall insert-in-minibuffer value)
	  (setq previous value))))))

(defun hf-file-name-directory (name)
  "Given a path, extract its directory part in such a way that
  any Emacs Tramp prefix of the form /protocol:user@host#port: is
  preserved."
  (if (string-match "/\\(.*\\):\\(.*\\):\\(.*\\)/\\(.*?\\)$" name)
      (concat "/" (match-string 1 name) ":" (match-string 2 name) ":" (match-string 3 name) "/")
    (if (string-match "/\\(.*\\):\\(.*\\):\\(.*\\)$" name)
	(concat "/" (match-string 1 name) ":" (match-string 2 name) ":")
      (file-name-directory name))))

(defun hf-truncate-file-name (name)
  "If the given file name contains a '~/', a '//' or a '../,
  immediately interpret these and return the resulting absolute
  path. This is Emacs Tramp transparent."
  (let* ((n1    (string-match (regexp-quote "~/") name))
	 (name1 (if n1 (file-name-as-directory (expand-file-name (substring name n1))) name))
	 (n2    (string-match (regexp-quote "//") name1))
	 (name2 (if n2 (expand-file-name (substring name1 (+ n2 1))) name1))
	 (n3    (string-match (regexp-quote "\.\./") name2))
	 (name3 (if n3 (expand-file-name name2) name2))
	 (n4    (string-match (regexp-quote "\./") name3))
	 (name4 (if n4 (expand-file-name name3) name3)))
    (if (string= name4 "")
	"/"
      name4)))

(defun hf-get-host-part (name)
  "Extract the host part '/protocol:user@host#port:' of
  a (potentially Emacs Tramp style) path <name>."
  (if (string-match "^/\\(.*\\):\\(.*\\):\\(.*\\)$" name)
      (concat "/" (match-string 1 name) ":" (match-string 2 name) ":")
    nil))

(defun hf-same-host-p (one two)
  "Test whether the (potentially Emacs Tramp style) paths <one> and
  <two> are on the same host."
  (string= (hf-get-host-part one) (hf-get-host-part two)))

(defun hf-make-relative-name (initial directory name)
  "All <name>s of recently opened files from recentf.el are
  processed through this function. They are expressed as paths
  relative to the <directory> part of the current helm-pattern.
  The behaviour of the present function depends on whether this
  is the <initial> run when the helm selection buffer just
  appears."
  (if (not (member 'recentf features))
      ;; If recentf has not been loaded, we never display any names of recently opened files.
      nil
    (if initial

	;; If this is the initial pattern, we do display every recently opened file.
	(if (hf-same-host-p directory name)

	    ;; If both files are on the same host, we express the <name> relative to <directory>.
	    (concat directory (file-relative-name name directory))

	  ;; If the files are on different hosts, we still display the prefix <director>, but then mark the correct file name with a " --> ".
	  (concat directory " --> " name))

      ;; If this is not the initial pattern, we display the names of recently opened files only if they are on the same host and share the same prefix.
      (if (hf-same-host-p directory name)
	  (let ((result (concat directory (file-relative-name name directory))))
	    (if (string-match "/\\.\\./" result)
		nil
	      result))
	nil))))

(defun hf-directory-files (d)
  "Return a list of all files and subdirectories of the given
  directory <d>. The subdirectories are identified by a trailing
  slash. This function is Emacs Tramp transparent and works even
  for directories <d> of the form
  '/protocol:user@host#port:path/file.ext'."
  (let ((path (hf-file-name-directory d))
	(non-essential nil)) ;; helm has suppressed Emacs Tramp during completion by setting this to t; since we need Tramp below, we temporarily set it to nil.
    ;; In order to obtain a list of files and subdirectories of <d>, we could
    ;; (let ((command (concat "ls -a1pL --group-directories-first " path)))
    ;;   (mapcar (lambda (x) (concat path x))
    ;;     (split-string (shell-command-to-string command) "\n" t)))
    ;; where the options to the 'ls' command serve the following purpose
    ;;	 -a  all files (including hidden ones)
    ;;	 -1  one file name per line, \n separated
    ;;	 -p  add / to all directory names
    ;;	 -L  display linked file rather than name of the symbolic link
    ;;
    ;; In order to make our helm selection of files work transparently in Emacs Tramp mode, however, we use the function
    ;; <directory-files-and-attributes> and take into account that its result is a list of lists with each list of the
    ;; form ("file name" <is_directory> ....)
    (if (file-directory-p path)
	(mapcar (lambda (x) (concat path (car x) (if (car (cdr x)) "/" ""))) (directory-files-and-attributes path))
      nil)))

(defun hf-resolve-arrow (name)
  "Resolve the arrow notation inrtoduced in
  <hf-make-relative-name>."
  (if (string-match "^\\(.*\\)\\ -->\\ \\(.*\\)$" name)
      (match-string 2 name)
    name))

(defun hf-action-find-file (candidate)
  "Open all files that the user has selected."
  (if (helm-marked-candidates :all-sources t)
      (mapc (lambda (x) (find-file (hf-resolve-arrow x))) (helm-marked-candidates :all-sources t))
    (find-file (hf-resolve-arrow candidate))))

(defun hf-action-grep (candidate)
  "Perform a helm-multi-occur or a ripgrep via helm-rg on the
  marked candidates."
  (interactive)
  (let ((markeds (mapcar (lambda (x) (hf-resolve-arrow x)) (helm-marked-candidates :all-sources t))))
    (if (member nil (mapcar (lambda (x) (file-directory-p x)) markeds))

	;; If there is an ordinary file among the marked candidates, run helm-multi-occur.
	;; If there are directories among the marked candidates, this will result in a grep over their dired buffers.
	;; Invoking helm-multi-occur-1 automatically works with Emacs Tramp file names.
	(helm-multi-occur-1 (mapcar (lambda (x) (find-file-noselect x)) markeds))

      ;; If the marked candidates are only directories, we can proceed only if all files are local because the two grep
      ;; methods do not work out of the box wih Emacs Tramp.
      (if (remove nil (mapcar (lambda (x) (hf-get-host-part x)) markeds))
	  (error "Some of the selected files are in remote locations. Running grep over Emacs Tramp has not yet been implemented.")

	(if (member 'helm-rg features)
	    ;; if helm-rg is installed, use it - it uses the command line tool ripgrep

	    (let* ((olddir helm-rg-default-directory)
		   (newdir (hf-common-prefix-list markeds))
		   (helm-rg-default-directory newdir))
	      (defun helm-rg--header-name (src) "" (concat "Recursive Grep in " helm-rg-default-directory))
	      (helm-rg nil nil (mapcar (lambda (x) (file-relative-name x newdir)) markeds))
	      (setq helm-rg-default-directory olddir))

	  ;; otherwise, use helm's built-in flavour of grep
	  (helm-do-grep-1 markeds t nil (list "*")))))))

(defun hf-find-files-grep ()
  "This is the helm key binding for the grep action."
  (interactive)
  (with-helm-alive-p
    (helm-exit-and-execute-action 'hf-action-grep)))

(defun hf-find-files-keymap ()
  "Return the helm key map for hf-find-files."
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map helm-map)
    (define-key map (kbd "C-s") 'hf-find-files-grep)
    map))

(defun hf-find-files ()
  "Relacement for helm-find-files with better behaviour under
  [TAB] completion and with the possibility of accessing history
  using the arrow keys."
  (interactive)
  (let* (
	 ;; the initial helm pattern is the directory in which to search
	 (pattern (hf-file-name-directory (expand-file-name (if buffer-file-name buffer-file-name default-directory))))

	 ;; the previous directory whose contants were suggested
	 (prevdir "")

	 ;; the most recently opened files
	 (recent (reverse recentf-list))

	 ;; the files in the relevant directory
	 (files '())

	 ;; all choices from these two sources
	 (choices '())

	 ;; local function that replaces the search string in the helm minibuffer
	 (insert-in-minibuffer (lambda (fname)
				 (with-selected-window (or (active-minibuffer-window) (minibuffer-window))
				   (delete-minibuffer-contents)
				   (set-text-properties 0 (length fname) nil fname)
				   (insert fname))))

	 ;; set the standard actions
	 (actions '(("Find File"  . hf-action-find-file)
		    ("Occur/Grep" . hf-action-grep)))

	 ;; remember the previous contents of the minibuffer when the [TAB] key was hit for the last time
	 (previous ""))

    (helm :sources (list (helm-build-sync-source "Recent Files"
			   :candidates (lambda ()
					 (last
					  (remove nil
						  (mapcar (lambda (x) (hf-make-relative-name (string= pattern helm-pattern) (hf-file-name-directory helm-pattern) x)) recent))
					       hf-recentf))
			   :keymap (hf-find-files-keymap)
			   :volatile t
			   :persistent-action #'(lambda (candidate) (hf-persistent-action candidate choices))
			   :action actions)
			 (helm-build-sync-source "File System"
			   :candidates (lambda ()
					 ;; This source is volatile, and so the candidates are recomputed whenever the <helm-pattern> (aka minibuffer contants) changes.
					 (let ((newpatt (hf-truncate-file-name helm-pattern))) ; First, we apply the truncation rules to the current <helm-pattern>.
					   (unless (string= helm-pattern newpatt)              ; If there has been any truncation, change the contents of the minibuffer accordingly.
					     (funcall insert-in-minibuffer newpatt))
					   (let ((newdir (hf-file-name-directory newpatt)))    ; The new directory whose contents become the candidates if the directory part of that pattern.
					     (when (not (string= prevdir newdir))              ; Only when the directory part has changed, we recompute the list of files and the total list of choices.
					       (setq prevdir newdir)
					       (setq files (hf-directory-files newdir))
					       (setq choices (append recent files)))
					     files)))
			   :keymap (hf-find-files-keymap)
			   :volatile t
			   :fuzzy-match t
			   :persistent-action #'(lambda (candidate) (hf-persistent-action candidate choices))
			   :action actions)
			 (helm-build-sync-source "Create File"
	                   :candidates (lambda ()
					 ;; This is a trivial volatile candidate. Whatever <helm-pattern> the user has typed now becomes a valid candidate.
					 ;; This way, there is always precisely one candidate identical to the <helm-pattern> in this source that serves as a fall-back when no other file has been found.
					 (list helm-pattern))
			   :keymap (hf-find-files-keymap)
                           :volatile t
			   :persistent-action #'(lambda (candidate) ())
                           :action actions))
	  :input pattern
	  :preselect (concat pattern "./")
	  :buffer "*helm files*")))

(provide 'helm-alt-find-files)

;;; ================================================================================
;;; End of file.
;;; ================================================================================
