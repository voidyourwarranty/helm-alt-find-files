# helm-alt-find-files : Replacement for helm-find-files that uses the [Tab] key in a way similar to bash

The Emacs [helm](https://github.com/emacs-helm/helm) completion framework by Thierry Volpiatto contains the function
`helm-find-files` as a replacement for vanilla `find-file`, often bound to [C-x][C-f].

I love helm, but when I type in a path or file name, thanks to decades of [Tab] completion in bash, I keep hitting
the [Tab] key hoping that it completes what I have typed as far as possible, but no, this goes against helm's
philosophy.

Shall I forget helm? No, it is too good to be dropped. Will I relearn the use of the [Tab] key? No, not after decades of
bash. So what gives? Helm gives. The present Emacs Lisp project contains a replacement for `helm-find-files` in which
the [Tab] key is tamed and retrained to do what I expect it to do.

# Features

## Finding Files

The function `hf-find-files` opens a helm session to select one or more files or directories. It begins with the
directory part of the file name associated with the current buffer, or if none is defined, with the default
directory. Starting from there, you can begin typing your path into the minibuffer.

Pressing [Tab] completes as much of the path as is uniquely specified given the contents of the minibuffer so far. You
can also type `//` as part of your file name which gets you to the root directory, `~/` which gets you to your home
directory or `/../` which gets you to the parent directory without interrupting the flow of typing, or `/./` which is
simply ignored.

At any point, you can use the cursor keys [Up] and [Down] in order to move to another helm candidate. If you press
[Tab] twice, the higlighted helm candidate is copied into the minibuffer where you can edit it further.

Helm is run in fuzzy matching mode, and so you can nail down the path name `foo-is-the-path/helm-is-a-cool-project.txt`
most likely by simply typing `fo` [Tab] `helm pro` [Enter] which is very convenient once you get used to it.

## Recent Files

If `recentf-mode` is turned on, above the standard helm source for finding files (called "File System"), another source
("Recent Files") is shown, displaying the most recently opened files as remembered by `recentf` with the most recent one
at the bottom. When `hf-find-files` is called, initially, the cursor is placed on the first line of the current
directory which is `some/path/./` specifying the directory itself. Now you can press [Up][Enter] to select the most
recent file or [Up][Up][Enter] to select the second most recent file, and so on.

This is precisely the behaviour of `find-file` in vanilla Emacs where [C-x][C-f][Up][Enter] opens the most recently
visited file.

The maximum number of recent files names shown is modified by adjusting `hf-recentf`.

## Selecting Files or Directories

When you select a helm candidate, it is passed to the standard `find-file` function, i.e. an ordinary file is opened in
a new buffer whereas a directory is opened in `dired` mode. If you select several candidates with [C-Space], each of
them is opened in this fashion.

## Tramp

All functions described so far can transparently use [Tramp](https://www.gnu.org/software/tramp/). When you type, for
example, `/ssh:user@host#port:` just after the second colon, when the Tramp prefix is complete, Emacs uses ssh in order
to connect to the other machine and reads completion candidates from there (possibly asking you for your ssh
password). Then it immediately offers the contents of the user's home directory on the remote machine as candidates, and
you can continue typing your path.

If some of the recent files as remembered by `recentf` are located on other machines, initially upon invoking
`hf-find-files` all of them are shown in their order of precedence. As helm would suppress them if they have the wrong
prefix, a `-->` notation is used in order indicate that they are rather located elsewhere. Once you begin typing,
though, the list of recent files is narrowed down as usual and only paths below the current point are suggested.

## Multi-Occur and Recursive Grep

In addition to [Tab] completion and finding files by pressing [Enter], there is one futher helm action, bound to [C-s].

If ordinary files and directories have been selected, it invokes `helm-multi-occur` which loads all selected files into
buffers and interactively searches all of them simultaneously. If there are directories among the selected files, they
are opened in `dired` mode and `helm-multi-occur` searches their `dired` representations. Applying `helm-multi-occur` in
this fashion transparently works with Tramp, i.e. the hosts on which the selected files are located, do not matter.

If only directories are selected, the action [C-s] invokes a recursive grep on these directories. If
[helm-rg](https://github.com/cosmicexplorer/helm-rg) has been loaded (which uses
[ripgrep](https://github.com/BurntSushi/ripgrep)), this is the first choice. Otherwise, we use the standard recursive
grep that is shipped with helm (`helm-do-grep-1`).

As of the present version, recursive grep does not yet work through Tramp and is thus limited to the local machine.

## Further Actions

In order to manupilate files and directories, I either use `dired` or a shell, and so I have not implemented all the
many actions that `helm-find-files` has.

# Configuration

In order to load `helm-alt-find-files`, you place the file `helm-alt-find-files.el` in the load-path and
```
(require 'helm-alt-find-files)
```

An example configuration with suggested key bindings is as follows,
```
(require 'recentf)                 ;; In helm-alt-find-files.el, the use of recentf-mode is optional.
(setq recentf-auto-cleanup 'never) ;; Make sure recentf does not stat all recent files on stratup because there may be Emacs Tramp files in there.
(recentf-mode 1)                   ;; Here we switch it on
(setq recentf-max-saved-items 25)  ;;   and remember the 25 most recently opened files.

(require 'tramp)                   ;; The use of Tramp mode is optional.

(require 'helm-alt-find-files)
(require 'helm-rg)                 ;; This package that relies on ripgrep is optional.

(global-set-key (kbd "C-x C-f") 'hf-find-files)
```
# Internals

## Name space

All variables and functions defined by `helm-alt-find-files` use the prefix `hf-`.

## Do I really need a replacement for `helm-find-files`?

My first step in adapting `helm-find-files` was to bind the persistent action to [Tab] and re-bind the action menu to [C-Tab], i.e.

```
(define-key helm-map (kbd "<tab>")   'helm-execute-persistent-action)
(define-key helm-map (kbd "<C-tab>") 'helm-select-action)
```

This gives the 'correct' flavour of [Tab] completion as soon as what you have typed so far uniquely specifies the next
subdirectory (this is because the stock persistent action uses the highlighted candidate to complete, and the
highlighted candidate is the first match which is hopefully unique). However, if whatever you have typed, does not
uniquely specifiy the next subdirectory,
+ bash completion would complete up to the point up to which file names are uniquely determined
+ but helm completes with the full highlighted candidate (which is the best match, but often not the only one).

This 'odd' behaviour of vanilla `helm-find-files` interrupted me often enough to warrant this project.

## Tasks and Suggestions

- so far Tramp prefixes with several hops may not work, fix the regexps
- make the grep actions work remotely with Tramp
- adjust the colors of helm-do-grep-1 to match those of helm-rg
- add a function hf-find-files-1 that allows to select (multiple|single) (files|directories) and simply returns their paths
- file names ought not contain '/', ':', '  -->  '
- select two files and immediately run ediff-buffers on them
- select several directories and then use their combined contents as candidates (hf-directory-files needs to work with list argument)

## License

[GNU General Public License GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
